#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define SIZE 10000

void print_kernel(int *kernel){
  for(int i = 0; i < 9; i++){
    printf("%d ", kernel[i]);
  }
  printf("\n");
}

void print_matrix(int *matrix){
  for(int i = 0; i < SIZE; i++){
    for (int j = 0; j < SIZE; j++){
      printf("%d ", matrix[i * SIZE + j]);
    }
    printf("\n");
  }
}

void compare_matrices(int *a, int *b){
  long int count = 0;
  for(int i = 0; i < SIZE * SIZE; i++){
    if (a[i] != b[i]){
      // printf("%d\n", i); // DEBUG
      count++;
    }
  }
  printf("Number of non-matching elements: %ld\n", count);
}

void convolution(int *out, int *in, int *kernel, int n){
  long int acc = 0;

  for (int k = 0; k < n; k++){
    // convolves with kernel k
    for(int i = 1; i < SIZE - 1; i++){
      for(int j = 1; j < SIZE - 1; j++){
        // convolving in the neighborhood of (i, j)
        acc = 0;
        for (int a = 0; a < 3; a++)
          for (int b = 0; b < 3; b++)
            acc += kernel[(9 * k) + (3 * a + b)] * in[SIZE * (i - 1 + a) + (j - 1 + b)];

        // sets the result mod 255
        out[(SIZE * i) + j] = acc % 255 + 1;
      }
    }

    // overwrites the results of conv(k) within in
    for (int i = 0; i < SIZE; i++)
      for (int j = 0; j < SIZE; j++)
        in[i * SIZE + j] = out[i * SIZE + j];
  }
}

void p_convolution(int *out, int *in, int *kernel, int n){
  long int acc = 0;
		
	#pragma omp target data map(tofrom: in[:SIZE*SIZE]) map(to: kernel[:n*9], out[:SIZE*SIZE])

		for (int k = 0; k < n; k++){
  		#pragma omp target teams distribute parallel for collapse(2) schedule(static, 1)
	    for(int i = 1; i < SIZE - 1; i++){
	      for(int j = 1; j < SIZE - 1; j++){
	        acc = 0;
	        for (int a = 0; a < 3; a++)
	          for (int b = 0; b < 3; b++)
	            acc += kernel[(9 * k) + (3 * a + b)] * in[SIZE * (i - 1 + a) + (j - 1 + b)];

	        out[(SIZE * i) + j] = acc % 255 + 1;
	      }
	    }
	
			#pragma omp target teams distribute parallel for collapse(2) schedule(static, 1)  
	    for (int i = 0; i < SIZE; i++)
	      for (int j = 0; j < SIZE; j++)
	        in[i * SIZE + j] = out[i * SIZE + j];
 	 	}
}


int main(int argc, char *argv[]){

  /* Part 1: input and initialization */
  int n_kernels;
  if (argc != 2){
    printf("Invalid args.\nUsage: convolution <no_of_kernels>\n");
    exit(1);
  }
  else {
    n_kernels = atoi(argv[1]);
  }

  // creating the kernels in a single matrix
  // the first kernel is indexed from kernel[0] to kernel[8] and so forth
  int *kernel = (int*) malloc(sizeof(int) * 9 * n_kernels);
  for (int k = 0; k < n_kernels; k++)
    for (int l = 0; l < 9; l++)
      kernel[k * 9 + l] = 1;


  // creating the input matrices
  int *in = malloc(sizeof(int) * SIZE * SIZE);
  int *p_in = malloc(sizeof(int) * SIZE * SIZE);
  for (int i = 0; i < SIZE * SIZE; i++){
    in[i] = 1;
    p_in[i] = 1;
  }

  /* Part 2: performing the convolutions */

  // SERIAL
  double s_start, s_end;
  s_start = omp_get_wtime();

  // an additional matrix is required to perform the convolution
  int *out = calloc(SIZE * SIZE, sizeof(int));
  convolution(out, in, kernel, n_kernels);

  s_end = omp_get_wtime();

  // PARALLEL
  double p_start, p_end;
  p_start = omp_get_wtime();

  // an additional matrix is required to perform the convolution
  int *p_out = calloc(SIZE * SIZE, sizeof(int));
  p_convolution(p_out, p_in, kernel, n_kernels);

  p_end = omp_get_wtime();

  /* Part 3: output */

    // DEBUG
     //print_matrix(in);
   //  printf("\n\n");
 //    print_matrix(p_in);

    compare_matrices(in, p_in);
    printf("Speedup: %.6lf\n", (s_end - s_start) / (p_end - p_start));

  return 0;
}
